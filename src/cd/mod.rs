use std::env;
use std::path::Path;
use std::str;

pub fn cd(dest: &str) {
    let root = Path::new(&dest);
    assert!(env::set_current_dir(&root).is_ok());
    println!("=>  Successfully changed working directory to {}!", root.display());
}
