use std::fs::File;
use std::os::unix::fs::PermissionsExt;

pub fn cm(mode: u32, file: &str) -> std::io::Result<()> {
    let f = File::open(file)?;
    let metadata = f.metadata()?;
    let mut permissions = metadata.permissions();

    permissions.set_mode(mode);
    assert_eq!(permissions.mode(), mode);
    Ok(())
}
