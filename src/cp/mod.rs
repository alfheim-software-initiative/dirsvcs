extern crate fs_extra;

use std::fs;
use fs_extra::dir::copy as cpd;
use fs_extra::dir::CopyOptions;
use fs_extra::error::*;

pub fn cp_dir(src: &str, dest: &str) -> Result<()> {
    let options = CopyOptions::new();
    cpd(src, dest, &options)?;

    Ok(())
}

pub fn cp_file(src: &str, dest: &str) -> std::io::Result<()> {
    fs::copy(src, dest)?;

    Ok(())
}
