use std::fs;
use std::io;
use std::io::Result;
use std::str;
use walkdir::WalkDir;

pub fn mkdir(dest: &str) -> Result<()> {
    fs::create_dir(&dest)?;
    Ok(())
}

pub fn rmdir(dir: &str) -> std::io::Result<()> {
    fs::remove_dir_all(dir)?;

    Ok(())
}

pub fn lsstr(s: &str) -> io::Result<()> {
    let walker = WalkDir::new(s).into_iter();
    for entry in walker {
        println!("{}", entry?.path().display());
    }

    Ok(())
}

pub fn rmstr(s: &str) -> Result<()> {
    fs::remove_file(&s)?;
    println!("Successfully removed {}", &s);

    Ok(())
}

